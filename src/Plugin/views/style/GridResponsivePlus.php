<?php
declare(strict_types=1);

namespace Drupal\views_css_grid\Plugin\views\style;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsStyle;
use Drupal\views\Plugin\views\style\GridResponsive;
use Drupal\Core\Form\FormStateInterface;

use Sabberworm\CSS\CSSList\Document;
use Sabberworm\CSS\Property\Selector;
use Sabberworm\CSS\Rule\Rule;
use Sabberworm\CSS\RuleSet\DeclarationBlock;

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;


#[ViewsStyle(
  id: "grid_responsive_plus",
  title: new TranslatableMarkup("Responsive Grid Plus"),
  help: new TranslatableMarkup("Displays rows in a responsive grid."),
  theme: "views_view_grid_responsive_plus",
  display_types: ["normal"],
)]
class GridResponsivePlus extends GridResponsive
{
  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = FALSE;

  /**
   * {@inheritdoc}
   */
  // protected $usesFields = TRUE;

  const CSS_AXES = ['inline', 'block'];
  
  const GRID_FLOW = ['align', 'justify'];
  const GRID_FLOW_VALUES = [
    'content' => ['start', 'center', 'end', 'space-between', 'space-around', 'space-evenly'],
    'items' => ['stretch', 'start', 'center', 'end', 'left', 'right', 'baseline']
  ];


  /**
  * {@inheritdoc}
  */
  protected function defineOptions(): array
  {
    $options = parent::defineOptions();
    $options['cell_min_width']['default'] = '30rem';

    $options = array_merge($options, [
      'inline' => [
        'contains' => [
          'column_gap' => ['default' => '1rem'],
          'justify_items' => ['default' => null],
          'justify_content' => ['default' => null],
        ]
      ],
      'block' => [
        'contains' => [
          'row_gap' => ['default' => '1rem'],
          'align_items' => ['default' => null],
          'align_content' => ['default' => null],
        ]
      ]
    ]);

    return $options;
  }

  private function buildAxisForm($axis)
  {
    $map = [
      'inline' => [
        'gap' => 'column_gap',
        'flow' => 'justify'
      ],
      'block' => [
        'gap' => 'row_gap',
        'flow' => 'align'
      ]
    ];

    $axis_form = [
      '#type' => 'fieldset',
    ];

    $axis_form[$map[$axis]['gap']] = [
      '#type' => 'textfield',
      '#title' => "Gap",
      '#attributes' => ['style' => 'width: 12em;'],
      '#default_value' => $this->options[$axis][$map[$axis]['gap']],
    ];

    foreach (self::GRID_FLOW_VALUES as $type => $values) {
      $property = "{$map[$axis]['flow']}_{$type}";

      $axis_form[$property] = [
        '#type' => 'select',
        '#title' => ucfirst($type),
        '#options' => array_combine($values, $values),
        '#default_value' => $this->options[$axis][$property],
        '#empty_option' => "- normal -"
      ];
    }

/*     $axis_form['masonry'] = [
      '#type' => 'checkbox',
      '#title' => 'Masonry',
      '#description' => $this->t("Items will load into the column with the most room causing a tightly packed layout without strict row tracks."),
      '#default_value' => $this->options['masonry']
    ]; */

    return $axis_form;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state): void
  {
    parent::buildOptionsForm($form, $form_state);
    unset($form['grid_gutter']);
    // @todo implement 'columns' feature from GridResponsive
    unset($form['columns']);

    $form['alignment']['#weight'] = 0;

    $form['help'] = [
      '#type' => 'link',
      '#title' => 'Css Grid Cheatsheet',
      '#url' => Url::fromUri('https://grid.malven.co/', [
        'attributes' => [
          'title' => $this->t("Opens a handy cheatsheet for css grid in a popup."),
          'onclick' => "window.open('https://grid.malven.co/','popup','width=960px,height=640px'); return false;",
          'target' => '_blank',
          'class' => ['button', 'button--primary']
        ]
      ]),
      '#weight' => -10
    ];

    $form['cell_min_width']['#type'] = 'textfield';
    $form['cell_min_width']['#attributes']['style'] = 'width: 12em;';
    unset($form['cell_min_width']['#field_suffix']);

    $form['inline'] = $this->buildAxisForm('inline');
    $form['inline']['#title'] = $this->t("Horizontal");
    
    $form['block'] = $this->buildAxisForm('block');
    $form['block']['#title'] = $this->t("Vertical");
  }

  public function getCss(): Document
  {
    $document = new Document();
    $block = new DeclarationBlock();
    $block->setSelectors([new Selector("")]);
    $document->append($block);

    $rule = new Rule('display');
    $rule->addValue('grid');
    $block->addRule($rule);

    switch($this->options['alignment']) {
      case 'horizontal':
        $rule = new Rule('grid-template-columns');
        $rule->addValue("repeat(auto-fit, minmax({$this->options['cell_min_width']}, 1fr))");
        break;
      case 'vertical':
        $rule = new Rule('grid-template-rows');
        $rule->addValue("repeat(auto-fit, minmax({$this->options['cell_min_width']}, 1fr))");
        break;
      default:
    }
    $block->addRule($rule);

    foreach (self::CSS_AXES as $axis) {
      foreach($this->options[$axis] as $property => $value) {
        if($value) {
          $rule = new Rule(Html::getClass($property));
          $rule->addValue($value);
          $block->addRule($rule);
        }
      }
    }

    return $document;
  }
}
